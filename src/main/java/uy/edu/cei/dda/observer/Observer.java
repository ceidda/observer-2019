package uy.edu.cei.dda.observer;

public interface Observer {

	public void update(String value);
	
}
