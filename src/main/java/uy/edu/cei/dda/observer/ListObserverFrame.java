package uy.edu.cei.dda.observer;

import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ListObserverFrame extends JFrame implements Observer {

	private JPanel contentPane;
	private DefaultListModel<String> model;
	private JTextArea textArea;
	private JTextField textField;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListObserverFrame listObserverFrame = new ListObserverFrame();
					listObserverFrame.showFrame();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListObserverFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);

		textArea = new JTextArea();
		sl_contentPane.putConstraint(SpringLayout.NORTH, textArea, -263, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, textArea, 4, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, textArea, -15, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, textArea, 212, SpringLayout.WEST, contentPane);
		contentPane.add(textArea);
		
		textField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, textField, 100, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, textField, 49, SpringLayout.EAST, textArea);
		contentPane.add(textField);
		textField.setColumns(10);

		model = new DefaultListModel<String>();
		model.addElement("test");
	}

	public void showFrame() {
		this.textArea.setText("test");
		this.textField.setText("t");
		this.setVisible(true);
	}

	public void update(String value) {
		this.textField.setText(value);
		String currentText = this.textArea.getText();
		this.textArea.setText(currentText + "\n" + value);
	}
}
