package uy.edu.cei.dda.observer;

import java.awt.EventQueue;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	System.out.println("window 1");
    	Observable window = new Observable();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window.showFrame();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		System.out.println("window 2");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListObserverFrame listObserverFrame = new ListObserverFrame();
					window.getObservers().add(listObserverFrame);
					listObserverFrame.showFrame();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
    }
}
